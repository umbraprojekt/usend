<?php
namespace UmbraProjekt\uSend;

/**
 * Factory that spawns Swift_Message instances
 */
class MessageFactory
{
	/**
	 * @var \Twig_Environment
	 */
	private $twig;

	/**
	 * @param \Twig_Environment $twig
	 */
	public function __construct(\Twig_Environment $twig)
	{
		$this->twig = $twig;
	}

	/**
	 * Create a new message
	 * @param  array          $postData User's input data
	 * @param  array          $mailData Data regarding the message's headers
	 * @return \Swift_Message
	 */
	public function spawn(array $postData, array $mailData)
	{
		$message = new \Swift_Message();

		foreach ($mailData as $header => $content) {
			switch ($header) {
				case "subject":
					$message->setSubject($this->twig->render($content, $postData));
					break;
				case "from":
					$message->setFrom($this->getAddresses($postData, $content));
					break;
				case "replyTo":
					$message->setReplyTo($this->getAddresses($postData, $content));
					break;
				case "to":
					$message->setTo($this->getAddresses($postData, $content));
					break;
				case "cc":
					$message->setCc($this->getAddresses($postData, $content));
					break;
				case "bcc":
					$message->setBcc($this->getAddresses($postData, $content));
					break;
				case "bodyHtml":
					$message->setBody($this->twig->render($content, $postData), "text/html");
					break;
				case "body":
					if (array_key_exists("bodyHtml", $mailData)) {
						$message->addPart($this->twig->render($content, $postData), "text/plain");
					} else {
						$message->setBody($this->twig->render($content, $postData), "text/plain");
					}
					break;
				default:
					throw new \InvalidArgumentException("Unrecognised header: {$header}.");
					break;
			}
		}

		return $message;
	}

	/**
	 * Get an array of email addresses from a given header containing one or multiple email addresses
	 * @param  array        $postData      User's input data
	 * @param  array|string $addressesData Message header containing the email address or addresses
	 * @return array
	 */
	private function getAddresses(array $postData, $addressesData) {
		$addresses = [];

		if (is_string($addressesData)) {
			$addresses[] = $this->twig->render($addressesData, $postData);
		} else if (is_array($addressesData)) {
			if (array_key_exists("name", $addressesData) && array_key_exists("email", $addressesData)) {
				$addresses[$this->twig->render($addressesData["email"], $postData)] = $this->twig
					->render($addressesData["name"], $postData);
			} else if (array_key_exists(0, $addressesData)) {
				foreach ($addressesData as $address) {
					$addresses += $this->getAddresses($postData, $address);
				}
			}
		}

		return $addresses;
	}
}
