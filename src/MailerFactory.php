<?php
namespace UmbraProjekt\uSend;

use UmbraProjekt\uSend\Config\ConfigInterface;

/**
 * Factory that spawns Swift_Mailer instances using different transports based on the transport config.
 */
class MailerFactory
{
	/**
	 * Make a new Swift_Mailer instance
	 * @param  ConfigInterface $config Transport configuration
	 * @return \Swift_Mailer
	 */
	public function spawn(ConfigInterface $config)
	{
		$transport = strtolower($config->get("transport"));
		if (!in_array($transport, ["smtp", "mail", "sendmail"])) {
			throw new \InvalidArgumentException("Invalid transport specified.");
		}

		return \Swift_Mailer::newInstance($this->$transport($config));
	}

	/**
	 *
	 * @param ConfigInterface $config
	 * @return \Swift_SmtpTransport
	 */
	private function smtp(ConfigInterface $config)
	{
		$transport = \Swift_SmtpTransport::newInstance();

		if ($config->has("host")) {
			$transport->setHost($config->get("host"));
		}

		if ($config->has("port")) {
			$transport->setPort($config->get("port"));
		}

		if ($config->has("encryption")) {
			$transport->setEncryption($config->get("encryption"));
		}

		if ($config->has("username")) {
			$transport->setUsername($config->get("username"));
		}

		if ($config->has("password")) {
			$transport->setPassword($config->get("password"));
		}

		return $transport;
	}

	/**
	 * Get sendmail transport instance
	 * @param  ConfigInterface          $config
	 * @return \Swift_SendmailTransport
	 */
	private function sendmail(ConfigInterface $config)
	{
		$transport = \Swift_SendmailTransport::newInstance();

		if ($config->has("command")) {
			$transport->setCommand($config->get("command"));
		}

		return $transport;
	}

	/**
	 * Get mail transport instance
	 * @param  ConfigInterface      $config
	 * @return \Swift_MailTransport
	 */
	private function mail(ConfigInterface $config)
	{
		$transport = \Swift_MailTransport::newInstance();

		if ($config->has("extraParams")) {
			$transport->setExtraParams($config->get("extraParams"));
		}

		return $transport;
	}
}
