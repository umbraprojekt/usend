<?php
namespace UmbraProjekt\uSend\Config;

/**
 * Base class for all config objects
 */
abstract class AbstractConfig implements ConfigInterface
{
	/**
	 * Parsed configuration data
	 * @var array
	 */
	protected $data;

	/**
	 * @inheritdoc
	 */
	public function get($field = null)
	{
		if (null === $field) {
			return $this->data;
		} else {
			return $this->data[$field];
		}
	}

	/**
	 * @inheritdoc
	 */
	public function has($field)
	{
		return array_key_exists($field, $this->data);
	}
}
