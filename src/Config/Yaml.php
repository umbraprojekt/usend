<?php
namespace UmbraProjekt\uSend\Config;

use Symfony\Component\Yaml\Parser;

/**
 * Configuration object created from a YAML config file
 */
class Yaml extends AbstractConfig
{
	/**
	 * @param string $configFile Path to the config file
	 */
	public function __construct($configFile)
	{
		$parser = new Parser();
		$this->data = $parser->parse(file_get_contents($configFile));
	}
}
