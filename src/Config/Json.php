<?php
namespace UmbraProjekt\uSend\Config;

/**
 * Configuration object created from a json config file
 */
class Json extends AbstractConfig
{
	/**
	 * @param string $configFile Path to the config file
	 */
	public function __construct($configFile)
	{
		$this->data = json_decode(file_get_contents($configFile), true);
	}
}
