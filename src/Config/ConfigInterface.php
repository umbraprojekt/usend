<?php
namespace UmbraProjekt\uSend\Config;

/**
 * Interface that all config objects must implement
 */
interface ConfigInterface
{
	/**
	 * Get the entire configuration data or a single field from it (shallow get!)
	 * @param  null|string $field Field name
	 * @return mixed
	 */
	public function get($field = null);

	/**
	 * Check whether the configuration data contains a given field (shallow check!)
	 * @param  string  $field Field name
	 * @return boolean
	 */
	public function has($field);
}
