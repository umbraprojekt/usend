<?php
namespace UmbraProjekt\uSend\Config;

/**
 * Configuration object created from a plain PHP array
 */
class PhpArray extends AbstractConfig
{
	/**
	 * @param array $config
	 */
	public function __construct(array $config)
	{
		$this->data = $config;
	}
}
