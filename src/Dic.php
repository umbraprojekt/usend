<?php
namespace UmbraProjekt\uSend;

use Pimple\Container;

/**
 * Dependency injection container class.
 * The uSend DIC contains a static instance of Pimple and is able to initialise it.
 */
class Dic
{
	/**
	 * @var Container
	 */
	private static $dic = null;

	/**
	 * Get the initialised static instance of Pimple
	 * @return Container
	 */
	public function getDic()
	{
		return (null === self::$dic ? $this->createDi() : self::$dic);
	}

	/**
	 * Create and initialise a Pimple instance
	 * @return Container
	 */
	private function createDi()
	{
		$dic = new Container();

		$dic["mailer_factory"] = function() {
			return new MailerFactory();
		};

		$dic["message_factory"] = function() use ($dic) {
			return new MessageFactory($dic["twig_environment"]);
		};

		$dic["twig_environment"] = function() use ($dic) {
			$loader = new \Twig_Loader_Chain([
				$dic["twig_filesystem_loader"],
				new \Twig_Loader_String()
			]);

			return  new \Twig_Environment($loader);
		};

		$dic["twig_filesystem_loader"] = function() {
			return new \Twig_Loader_Filesystem();
		};

		self::$dic = $dic;

		return self::$dic;
	}
}
