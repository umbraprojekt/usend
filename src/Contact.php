<?php
namespace UmbraProjekt\uSend;

use UmbraProjekt\uSend\Config\ConfigInterface;
use Zend\Validator\ValidatorInterface;

/**
 * The contact class is the main entry point for the application. By creating an instance, it is possible to perform
 * input validation and to send out emails.
 */
class Contact
{
	const VERSION = "1.1.0";

	/**
	 * Parsed configuration
	 * @var ConfigInterface
	 */
	private $config;

	/**
	 * Result of the contact form processing
	 * @var array
	 */
	private $result;

	/**
	 * @var \Swift_Mailer
	 */
	private $mailer;

	/**
	 * @var MessageFactory
	 */
	private $messageFactory;

	/**
	 * Attachments
	 * @var \Swift_Mime_Attachment[]
	 */
	private $attachments = [];

	/**
	 * @param Dic             $dic           Dependency injection container
	 * @param ConfigInterface $transport     Mail transport config
	 * @param ConfigInterface $config        Validation and email recipients config
	 * @param string          $templatesPath Base directory to which all template files in config are relative to
	 */
	public function __construct(Dic $dic, ConfigInterface $transport, ConfigInterface $config, $templatesPath = null)
	{
		$this->config = $config;
		$this->mailer = $dic->getDic()["mailer_factory"]->spawn($transport);
		$this->messageFactory = $dic->getDic()["message_factory"];

		// set base directory for templates
		if ($templatesPath) {
			$dic->getDic()["twig_filesystem_loader"]->addPath($templatesPath);
		}
	}

	/**
	 * Reset the result property value.
	 */
	private function resetResult()
	{
		$this->result = [
			"success" => true,
			"errors" => []
		];
	}

	/**
	 * @param string $field         The field that didn't pass the validation
	 * @param string $validatorName The validator name that gave an invalid result
	 */
	private function error($field, $validatorName)
	{
		$this->result["success"] = false;
		$this->result["errors"][$field][] = $validatorName;
	}

	/**
	 * Run the input validation and mail sending
	 * @param  array $input The input data array (usually POST data)
	 * @return array        Validation results
	 */
	public function run(array $input)
	{
		$this->resetResult();

		if ($this->isValid($input)) {
			$this->sendEmails($input);
		}

		return $this->result;
	}

	/**
	 * @param  array   $input
	 * @return boolean
	 */
	private function isValid(array $input)
	{
		$validation = $this->config->get("validation");
		if (is_array($validation)) {
			foreach ($validation as $field => $validators) {
				foreach ($validators as $validatorName => $options) {
					// empty options object must be hanged to array to avoid exceptions
					if (!$options) {
						$options = [];
					}

					$validatorClass = 'Zend\Validator\\' . $validatorName;
					/**
					 * @var ValidatorInterface $validator
					 */
					$validator = new $validatorClass($options);
					if (!$validator->isValid($input[$field])) {
						$this->error($field, $validatorName);
					}
				}
			}
		}

		return $this->result["success"];
	}

	/**
	 * Add an attachment to the attachments pool.
	 * The attachments will be added to each sent message unless disabled in the config.
	 * @param  \Swift_Mime_Attachment|string $attachment Attachment file name or object
	 * @return Contact
	 * @throws \Exception
	 */
	public function withAttachment($attachment)
	{
		if (is_string($attachment)) {
			$this->addAttachmentFromFile($attachment);
		} else if ($attachment instanceof \Swift_Mime_Attachment) {
			$this->attachments[] = $attachment;
		} else {
			throw new \InvalidArgumentException("Attempted to add an invalid attachment. Expected filename or "
				. "Swift_Mime_Attachment instance.");
		}

		return $this;
	}

	/**
	 * @param  string     $attachment Attachment file name
	 * @throws \Exception
	 */
	private function addAttachmentFromFile($attachment) {
		if (!is_file($attachment)) {
			throw new \Exception("Tried to attach a nonexistent file: {$attachment}");
		}

		$this->attachments[] = \Swift_Attachment::fromPath($attachment);
	}

	/**
	 * @param array $input
	 */
	private function sendEmails(array $input)
	{
		foreach ($this->config->get("email") as $mailData) {
			$message = $this->messageFactory->spawn($input, $this->getMessageHeaders($mailData));

			if (!array_key_exists("disableAttachments", $mailData) || !$mailData["disableAttachments"]) {
				foreach ($this->attachments as $attachment) {
					$message->attach($attachment);
				}
			}

			$this->mailer->send($message);
		}
	}

	/**
	 * Extract only the message headers from the message config array
	 * @param  array $messageData
	 * @return array              Message headers
	 */
	private function getMessageHeaders(array $messageData)
	{
		$headers = ["body", "bcc", "bodyHtml", "cc", "from", "replyTo", "subject", "to"];
		$output = [];
		foreach($headers as $header) {
			if (array_key_exists($header, $messageData)) {
				$output[$header] = $messageData[$header];
			}
		}

		return $output;
	}
}
