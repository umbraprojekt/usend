<?php
namespace UmbraProjekt\uSend\Config;

class YamlTest extends \PHPUnit_Framework_TestCase
{
	/**
	 * Config must be read properly from YAML files
	 */
	public function testReadConfig()
	{
		$config = new Yaml(__DIR__ . "/../resources/config.yml");

		$reflection = new \ReflectionProperty('UmbraProjekt\uSend\Config\Yaml', "data");
		$reflection->setAccessible(true);
		$data = $reflection->getValue($config);

		$this->assertArrayHasKey("validation", $data);
		$this->assertArrayHasKey("email", $data);
		$this->assertEquals(15, $data["validation"]["content"]["StringLength"]["min"]);
	}
}
