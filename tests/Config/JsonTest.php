<?php
namespace UmbraProjekt\uSend\Config;

class JsonTest extends \PHPUnit_Framework_TestCase
{
	/**
	 * Config must be read properly from json files
	 */
	public function testReadConfig()
	{
		$config = new Json(__DIR__ . "/../resources/config.json");

		$reflection = new \ReflectionProperty('UmbraProjekt\uSend\Config\Json', "data");
		$reflection->setAccessible(true);
		$data = $reflection->getValue($config);

		$this->assertArrayHasKey("validation", $data);
		$this->assertArrayHasKey("email", $data);
		$this->assertEquals(15, $data["validation"]["content"]["StringLength"]["min"]);
	}
}
