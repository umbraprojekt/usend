<?php
namespace UmbraProjekt\uSend\Config;

class PhpArrayTest extends \PHPUnit_Framework_TestCase
{
	private $config = [
		"validation" => [
			"name" => [
				"NotEmpty" => [],
				"StringLength" => [
					"min" => 3
				]
			],
			"email" => [
				"NotEmpty" => [],
				"EmailAddress" => []
			],
			"content" => [
				"NotEmpty" => [],
				"StringLength" => [
					"min" => 15
				]
			]
		],
		"email" => [
			[
				"bodyHtml" => "some/template.twig",
				"subject" => "Contact from {{name}} at example.com",
				"from" => "noreply@example.com",
				"replyTo" => "{{email}}",
				"to" => [
					[
						"name" => "John Doe",
						"email" => "john@example.com"
					],
					[
						"name" => "Jane Doe",
						"email" => "jane@example.com"
					]
				]
			],
			[
				"bodyHtml" => "some/template2.twig",
				"subject" => "Email sent from contact form at example.com",
				"from" => [
					"name" => "Example.com mailer",
					"email" => "noreply@example.com"
				],
				"to" => [
					[
						"name" => "{{name}}",
						"email" => "{{email}}"
					]
				]
			]
		]
	];

	/**
	 * Config must be read properly from json files
	 */
	public function testReadConfig()
	{
		$config = new PhpArray($this->config);

		$reflection = new \ReflectionProperty('UmbraProjekt\uSend\Config\PhpArray', "data");
		$reflection->setAccessible(true);
		$data = $reflection->getValue($config);

		$this->assertArrayHasKey("validation", $data);
		$this->assertArrayHasKey("email", $data);
		$this->assertEquals(15, $data["validation"]["content"]["StringLength"]["min"]);
	}

	public function testGetConfig()
	{
		$config = new PhpArray($this->config);

		$this->assertEquals($this->config, $config->get());
		$this->assertEquals($this->config["validation"], $config->get("validation"));
	}

	public function testHasConfig()
	{
		$config = new PhpArray($this->config);

		$this->assertTrue($config->has("validation"));
		$this->assertFalse($config->has("lumberjack"));
	}
}
