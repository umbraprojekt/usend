<?php
namespace UmbraProjekt\uSend;

use Mockery\MockInterface;

class MessageFactoryTest extends \PHPUnit_Framework_TestCase
{
	/**
	 * @var MessageFactory
	 */
	private $factory;

	/**
	 * @var MockInterface
	 */
	private $twig;

	public function setUp()
	{
		$this->twig = \Mockery::mock("Twig_Environment");
		$this->factory = new MessageFactory($this->twig);

		$this->twig->shouldReceive("render")->andReturnUsing(function($template, $vars) {
			return $template;
		});
	}

	public function tearDown()
	{
		\Mockery::close();
	}

	/**
	 * Addresses must be processed correctly
	 */
	public function testAddressProcessing()
	{
		$reflectionAddress = new \ReflectionMethod('UmbraProjekt\uSend\MessageFactory', "getAddresses");
		$reflectionAddress->setAccessible(true);

		$this->assertSame([
			"john@example.com" => "John"
		], $reflectionAddress->invoke($this->factory, [], [
			"name" => "John",
			"email" => "john@example.com"
		]));
		$this->assertSame([
			"noreply@example.com"
		], $reflectionAddress->invoke($this->factory, [], "noreply@example.com"));
		$this->assertSame([
			"john@example.com" => "John",
			"jane@example.com" => "Jane",
			"noreply@example.com"
		], $reflectionAddress->invoke($this->factory, [], [
			["name" => "John", "email" => "john@example.com"],
			["name" => "Jane", "email" => "jane@example.com"],
			"noreply@example.com"
		]));
	}

	/**
	 * Messages must spawn correctly for correct input
	 */
	public function testMessageSpawn()
	{
		$mailData = [
			"from" => ["name" => "mailer", "email" => "noreply@example.com"],
			"replyTo" => ["name" => "John", "email" => "john@example.com"],
			"to" => ["name" => "Jane", "email" => "jane@example.com"],
			"cc" => ["name" => "Boss", "email" => "boss@example.com"],
			"bcc" => ["name" => "Mum", "email" => "mum@example.com"],
			"subject" => "Hello world",
			"body" => "Hello, this is a test email!"
		];
		$postData = [];
		$message = $this->factory->spawn($postData, $mailData);

		$this->assertInstanceOf("Swift_Message", $message);
		$this->assertSame(["noreply@example.com" => "mailer"], $message->getFrom());
		$this->assertSame(["john@example.com" => "John"], $message->getReplyTo());
		$this->assertSame(["jane@example.com" => "Jane"], $message->getTo());
		$this->assertSame(["boss@example.com" => "Boss"], $message->getCc());
		$this->assertSame(["mum@example.com" => "Mum"], $message->getBcc());
		$this->assertEquals("Hello, this is a test email!", $message->getBody());
		$this->assertEquals("Hello world", $message->getSubject());
	}

	/**
	 * Do not accept odd headers
	 */
	public function testBadHeader()
	{
		$this->setExpectedException("InvalidArgumentException");

		$mailData = [
			"bad" => "I am a bad header"
		];
		$postData = [];
		$this->factory->spawn($postData, $mailData);
	}
}
