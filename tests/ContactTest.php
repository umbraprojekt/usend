<?php
namespace UmbraProjekt\uSend;

use UmbraProjekt\uSend\Config\Json;
use UmbraProjekt\uSend\Config\Yaml;

class ContactTest extends \PHPUnit_Framework_TestCase
{
	public function validationDataProvider()
	{
		return [
			[
				[
					"name" => "Lumberjack",
					"email" => "lumber@jack.com",
					"content" => "I'm a lumberjack and I'm OK"
				],
				true,
				[
					"success" => true,
					"errors" => []
				]
			],
			[
				[
					"name" => "",
					"email" => "invalid",
					"content" => "short"
				],
				false,
				[
					"success" => false,
					"errors" => [
						"name" => ["NotEmpty", "StringLength"],
						"email" => ["EmailAddress"],
						"content" => ["StringLength"]
					]
				]
			]
		];
	}

	/**
	 * @param $input
	 * @param $expectedReturnValue
	 * @param $expectedResult
	 *
	 * @dataProvider validationDataProvider
	 */
	public function testValidation($input, $expectedReturnValue, $expectedResult)
	{
		$contact = new Contact(
			new Dic(),
			new Yaml(__DIR__ . "/resources/transport.yml"),
			new Json(__DIR__ . "/resources/config.json")
		);

		$reflectionReset = new \ReflectionMethod('UmbraProjekt\uSend\Contact', "resetResult");
		$reflectionReset->setAccessible(true);
		$reflectionReset->invoke($contact);

		$reflectionValidate = new \ReflectionMethod('UmbraProjekt\uSend\Contact', 'isValid');
		$reflectionValidate->setAccessible(true);
		$returnValue = $reflectionValidate->invoke($contact, $input);

		$reflectionResult = new \ReflectionProperty('UmbraProjekt\uSend\Contact', "result");
		$reflectionResult->setAccessible(true);
		$result = $reflectionResult->getValue($contact);

		$this->assertEquals($expectedReturnValue, $returnValue);
		$this->assertSame($expectedResult, $result);
	}

	public function testShouldAttachFiles()
	{
		$dic = new Dic();

		$mailerFactory = \Mockery::mock('UmbraProjekt\uSend\MailerFactory')
			->makePartial();
		$mailerFactory->shouldReceive("spawn->send")->with(\Mockery::on(function($message) {
			$messageAsString = $message->toString();
			$attachmentContent = base64_encode(file_get_contents(__DIR__ . "/resources/attachment.txt"));
			return strpos($messageAsString, $attachmentContent) !== false;
		}));
		$dic->getDic()->offsetUnset("mailer_factory");
		$dic->getDic()["mailer_factory"] = function() use ($mailerFactory) {
			return $mailerFactory;
		};

		$contact = new Contact(
			$dic,
			new Yaml(__DIR__ . "/resources/transport.yml"),
			new Yaml(__DIR__ . "/resources/config-attachment.yml")
		);

		$contact->withAttachment(__DIR__ . "/resources/attachment.txt")
			->run([]);

		\Mockery::close();
	}

	public function testShouldNotAttachFiles()
	{
		$dic = new Dic();

		$mailerFactory = \Mockery::mock('UmbraProjekt\uSend\MailerFactory')
			->makePartial();
		$mailerFactory->shouldReceive("spawn->send")->with(\Mockery::on(function($message) {
			$messageAsString = $message->toString();
			$attachmentContent = base64_encode(file_get_contents(__DIR__ . "/resources/attachment.txt"));
			return strpos($messageAsString, $attachmentContent) === false;
		}));
		$dic->getDic()->offsetUnset("mailer_factory");
		$dic->getDic()["mailer_factory"] = function() use ($mailerFactory) {
			return $mailerFactory;
		};

		$contact = new Contact(
			$dic,
			new Yaml(__DIR__ . "/resources/transport.yml"),
			new Yaml(__DIR__ . "/resources/config-no-attachment.yml")
		);

		$contact->withAttachment(__DIR__ . "/resources/attachment.txt")
			->run([]);

		\Mockery::close();
	}
}
