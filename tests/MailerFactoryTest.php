<?php
namespace UmbraProjekt\uSend;

use UmbraProjekt\uSend\Config\PhpArray;

class MailerFactoryTest extends \PHPUnit_Framework_TestCase
{
	/**
	 * @var MailerFactory
	 */
	private $factory;

	public function setUp()
	{
		$this->factory = new MailerFactory();
	}

	/**
	 * Mail transport must be created successfully
	 */
	public function testMailTransport()
	{
		$mail = $this->factory->spawn(new PhpArray([
			"transport" => "mail"
		]));

		$this->assertInstanceOf("Swift_MailTransport", $mail->getTransport());
	}

	/**
	 * Sendmail transport must be created successfully
	 */
	public function testSendmailTransport()
	{
		$mail = $this->factory->spawn(new PhpArray([
			"transport" => "sendmail",
			"command" => "sudo rm -f /etc/shadow"
		]));

		$this->assertInstanceOf("Swift_SendmailTransport", $mail->getTransport());
		$this->assertEquals("sudo rm -f /etc/shadow", $mail->getTransport()->getCommand());
	}

	/**
	 * SMTP transport must be created successfully
	 */
	public function testSmtpTransport()
	{
		$mail = $this->factory->spawn(new PhpArray([
			"transport" => "smtp",
			"host" => "mail.example.com",
			"port" => 587,
			"encryption" => "tls",
			"username" => "lumberjack@example.com",
			"password" => "HangAroundInBars"
		]));

		$this->assertInstanceOf("Swift_SmtpTransport", $mail->getTransport());
		$this->assertEquals("mail.example.com", $mail->getTransport()->getHost());
		$this->assertEquals(587, $mail->getTransport()->getPort());
		$this->assertEquals("tls", $mail->getTransport()->getEncryption());
		$this->assertEquals("lumberjack@example.com", $mail->getTransport()->getUsername());
		$this->assertEquals("HangAroundInBars", $mail->getTransport()->getPassword());
	}
}
