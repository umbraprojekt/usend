# uSend

uSend is a tool that helps you work with contact forms: it performs input data validation and sends out emails. All
controlled by simple config files and a template or two.

## Installation

Install uSend using Composer:

    composer require umbraprojekt/usend

Then, in your project, require Composer's autoloader if you haven't done so already:

    <?php
    require_once "../vendor/autoload.php";

## Using uSend

### Instantiation

The whole idea behind uSend is to simplify the process of validating forms and sending emails as much as possible. It is
all done using two configuration files (or arrays, or a mix thereof), JSON or YAML. The library's main object also
requires that you pass it an instance of uSend's dependency injection container. The instantiation is done like this:

    <?php
    use UmbraProjekt\uSend\Contact;
    use UmbraProjekt\uSend\Dic;
    use UmbraProjekt\uSend\Config\Yaml;
    
    $uSend = new Contact(
        new Dic(),
        new Yaml("/path/to/transport.yml"),
        new Yaml("/path/to/config.yml"),
        "/path/to/templates"
    );

The first config file or array contains the transport data. Transport data tells uSend what mail transport method to
use: PHP's `mail()` function, UNIX `sendmail` command or SMTP. The second config contains user input validation and
email configuration (what mail or mails need to be sent, their content and headers).

Finally, the last argument is optional and it contains the base path for the template files that are defined in the
configuration file. It's not required if the template paths are absolute.

### Performing validation and sending out emails

The validation and mail sending are performed behind the scenes and are transparent to the developer (provided the
configuration is passed in). It's done by calling the `run()` method of the `Contact` instance and passing it the user
input data:

    <?php
    $input = json_decode(file_get_contents("php://input"), true);
    $input["ip"] = $_SERVER["CLIENT_ADDR"]; // we add data!
    
    $result = $uSend->run($input);

The result that is returned is an array with two indices:

* `success` - a boolean value indicating whether the validation was successful and the mails were sent
* `errors` - an array of validation errors

The validation errors will be an associative array where validated field names contain arrays of validator names that
returned invalid results. For instance, if the `message` field had a `StringLength` validator that returned an invalid
result (e.g. the message was too short), the response might look like this:

    <?php
    [
        "success" => true,
        "errors" => [
            "message" => [
                "StringLength"
            ]
        ]
    ]

## Config objects

There are various ways configuration can be passed to config objects, depending on what your personal preference is.

### PhpArray

This is the simplest way of introducing configuration to uSend, but also the least flexible, as the config needs to live
directly in the code.

    <?php
    use UmbraProjekt\uSend\Config\PhpArray;
    
    $config = new PhpArray([
        "foo" => "bar"
    ]);

### Yaml

In order to create the confg object, pass it the path to a yml config file:

    <?php
    use UmbraProjekt\uSend\Config\Yaml;
    
    $config = new Yaml("/path/to/config.yml");

### Json

Similar to `Yaml`, it's enough to pass it the path to a json config file:

    <?php
    use UmbraProjekt\uSend\Config\Json;
    
    $config = new Json("/path/to/config.json");

## Transport configuration

Transpor config file can look very differently depending on what mail transport is used. In case of mail and sendmail
transports, the config file will usually look like either of these:

    transport: mail

or

    transport: sendmail

In case of SMTP, the configuration can be a bit longer and contains a few extra options.

### mail

The `mail` transport has only one additional optional parametre: `extraParams`. These are additional parametres passed
to the `mail()` function (its fifth parametre).

Example YAML config:

    transport: mail
    extraParams: "-f%s"

### sendmail

As with `mail`, `sendmail` is configured with no or minimal additional options. There is only one additional parametre,
which is entirely optional: `command`. It contains the command to be executed in order to send an the email.

Example YAML config:

    transport: sendmail
    command: "/usr/bin/sendmail -bs"

### SMTP

SMTP comes with the most configuration options. Here are the available options:

* `host` - the host from which the mail will be sent out. Defaults to `localhost`
* `password` - the password of th euser sending out the email
* `port` - the port to be used when communicating with the host. Defaults to `25`
* `encryption` - encryption type (`ssl`, `tls`...). Defaults to no encryption at all
* `username` - username of the user sending out the email

Example YAML config:

    transport: smtp
    host: smtp.google.com
    port: 487
    encryption: ssl
    username: user@gmail.com
    password: superSecretPassword

## Mail configuration

The mail configuuration contains two main sections: `validation` and `email`.

### Validation

The validation config assigns validators to form fields. Assuming the form sends three fields: `name`, `email` and
`message`, the validation may include sections for each of these fields.

Every validated field in turn has keys for validator names. The keys correspond to the validators in Zend Framework's
package. The most used will probably be `NotEmpty`, `StringLength` and `EmailAddress`.

Each validator needs to be configured. When no configuration needs to be passed in, just leave  the key with no value.
Otherwise, the value is an object corresponding exatcly to the array that will be passed to the validator's constructor.

Example YAML config:

    validation:
        name:
            NotEmpty:
        email:
            EmailAddress:
        message:
            StringLength:
                min: 15

The above will validate three fields (of course, there may be others that don't necessarily require validation) and it
will use three validators: the `NotEmpty` validator with default config, `EmailAddress`, also with default config, and
finally `StringLength`, which will be passed an array with the key `min` and value `15`, creating a validator that
checks whether the `message` key of the user's input has at least 15 characters. Please refer to Zend Framework's
validators package for information about the available validators and their config.

### Email

Email config contains an array of messages that will be sent if and only if the validation passes (or when no validation
is required at all). Under each key, it is possible to define multiple fields (not all are required) that correspond to
the email body, headers or additional options. Note that all fields declared here are treated as Twig templates, so you
can use Twig's syntax to escape fields from the user input and use filters. For example, if you decide to send the
person who filled the contact form a confirmation message, you may configure their email address to be whatever address
they provided in the contact form:

    to: "{{ email }}"

Also bear in mind that instead of a string parseable by Twig, you may also specify a path to a Twig template file. In
such a case you are required to either provide an absolute path or a path relative to the templates root directory,
provided to the constructor of the `UmbraProjekt\uSend\Contact` object:

    body: my_awesome_template.twig

Available fields for each email are the following:

* `body` - the message's text body
* `bcc` - the message's BCC address or addresses
* `bodyHtml` - the message's HTML body
* `cc` - the message's CC address or addresses
* `from` - address or addresses from which the message is send
* `replyTo` - address to be used when replying to the email
* `subject` - the message's subject
* `to` - message's recipient or recipients

Available extra options:

* `disableAttachments` - flag used to block sending attachments with a given message

#### Defining email addresses

The email addresses in the `from`, `to`, `replyTo`, `cc` and `bcc` fields can be defined in more than one way.

Each individual email can either be a string containing just the address or an object containing two mandatory fields:
`name` and `email`:

    from: "noreply@example.com"
    to: { name: John Doe, email: "recipient@example.com" }
    replyTo: { name: "{{ name }}", email: "{{ email }}" }

If there are multiple addresses, they are defined as an array. Again, each array item can either be a string or an
object:

    to:
        - { name: John Doe, email: "recipient@example.com" }
        - "another.recipient@example.com"
